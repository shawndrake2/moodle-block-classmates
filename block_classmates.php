<?php //$Id: block_classmates.php,v 1 2010/02/04 Drake $

class block_classmates extends block_base {
	
    function init() {
        $this->title = 'Classmates';
        $this->version = 2010020400;
    }

    function hide_header() {
	  return false;
	}
	
	function get_content() {
        global $USER, $CFG, $COURSE;
		
		echo '<script type="text/javascript">  
				function swapImage(div, image, text) {
					if (document.getElementById(div).style.display == "none"){
						document.getElementById(div).style.display = "block"
						document.getElementById(image).src="'.$CFG->wwwroot.'/theme/ECPI/pix/mod/glossary/minus.gif"
						document.getElementById(image).alt="hide"
						document.getElementById(image).title="Hide " + text
						delete_cookie (\''.$USER->username.'_'.$COURSE->shortname.'_\' + div)
					}else{
						document.getElementById(div).style.display = "none"
						document.getElementById(image).src="'.$CFG->wwwroot.'/theme/ECPI/pix/mod/glossary/export.gif"
						document.getElementById(image).alt="show"
						document.getElementById(image).title="Show " + text
						setCookie(\''.$USER->username.'_'.$COURSE->shortname.'_\' + div, \'closed\')
					}
				}
				function setCookie(name, value){
					document.cookie = name + \'=\' + value 	
					if (name == \''.$USER->username.'_'.$COURSE->shortname.'_sort\'){
						window.location.reload()
					}
				}
				function delete_cookie ( cookie_name ) 
				{ 
					var cookie_date = new Date ( );  // current date & time 
					cookie_date.setTime ( cookie_date.getTime() - 1 ); 
					document.cookie = cookie_name += "=; expires=" + cookie_date.toGMTString(); 
				} 
				function submitform(url, name)
				{
				  newwindow=window.open(url,name,\'height=500,width=500\');
				  if (window.focus){
				   	newwindow.focus()
				  }
         		  document.messages.target=name;
				  document.messages.submit() ;
				}

				window.name = \'course_page\'

				</script>
				';

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        if (empty($this->instance)) {
            return $this->content;
        }
		
		$cookie_sort = var_export($_COOKIE[$USER->username.'_'.$COURSE->shortname.'_sort'], true);
		$cookie_dis_students = var_export($_COOKIE[$USER->username.'_'.$COURSE->shortname.'_dis_students'], true);
		$cookie_students = var_export($_COOKIE[$USER->username.'_'.$COURSE->shortname.'_students'], true);
		$cookie_teachers = var_export($_COOKIE[$USER->username.'_'.$COURSE->shortname.'_teachers'], true);

        // Get context so we can check capabilities.
        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);

        if ($COURSE->id !== SITEID) {  // Site-level
		
			$hiddenpic = 'Ninja.png';
		
			//get site_wide disabled students first
			$site_dlist = array();
			$sql = 'SELECT u.id as id
			FROM mdl_role_assignments ra, mdl_user u, mdl_context cxt, mdl_role r
			WHERE ra.userid = u.id
			AND r.id = ra.roleid
			AND ra.contextid = cxt.id
			AND cxt.contextlevel = 10
			AND r.shortname = "disabled"';
			$results = get_records_sql($sql);
			foreach($results as $site_disabled){
				$site_dlist[] = $site_disabled->id;
			}
			
			//array of disabled users in this course
			$dlist = array();
			$sql = 'SELECT u.id as id
					FROM mdl_role_assignments ra, mdl_user u, mdl_course c, mdl_context cxt, mdl_role r
					WHERE ra.userid = u.id
					AND r.id = ra.roleid
					AND ra.contextid = cxt.id
					AND cxt.contextlevel =50 
					AND cxt.instanceid = c.id
					AND c.id = '.$COURSE->id.'
					';
			$role = ' AND r.shortname = "disabled"';
			$results = get_records_sql($sql.$role);
			foreach($results as $disabled){
				$dlist[] = $disabled->id;
			}

			//set up query to use everywhere else
			$sql = 'SELECT u.id, u.username as username, u.firstname, u.lastname, u.picture, ra.hidden as hidden, r.shortname as role
					FROM mdl_role_assignments ra, mdl_user u, mdl_course c, mdl_context cxt, mdl_role r
					WHERE ra.userid = u.id
					AND r.id = ra.roleid
					AND ra.contextid = cxt.id
					AND cxt.contextlevel =50 
					AND cxt.instanceid = c.id
					AND c.id = '.$COURSE->id.'
					';
			if (!has_capability('moodle/role:viewhiddenassigns', $context)) {	
				$sql .= ' AND ra.hidden = 0';
			}
			if ((isset($cookie_sort) && ($cookie_sort == '\'firstname\'')) || (!has_capability('moodle/course:viewhiddencourses', $context))){
				$order = ' ORDER BY u.firstname';
			}else{
				$order = ' ORDER BY u.lastname';
			}
			
			//initialize lists
			$disabled_list = '<li>&nbsp;</li><lh><strong><u>Disabled Students</u></strong><img id="image3" src="'.$CFG->wwwroot.'/theme/ECPI/pix/mod/glossary/';
			if (isset($cookie_dis_students) && ($cookie_dis_students == '\'closed\'')){
				$disabled_list .= 'export';
			}else{
				$disabled_list .= 'minus';
			}
			$disabled_list .= '.gif" alt="hide" title="Hide Disabled Students" style="display:block; float:right;" onClick="swapImage(\'dis_students\', \'image3\', \'Disabled Students\');"></lh><div id="dis_students" style="display:';
			if (isset($cookie_dis_students) && ($cookie_dis_students == '\'closed\'')){
				$disabled_list .= 'none';
			}else{
				$disabled_list .= 'block';
			}
			$disabled_list .= ';"><li>&nbsp;</li>';
			if(has_capability('moodle/course:viewhiddencourses', $context)){
				$active_list = '<lh><strong><u>Active Students';
			}else{
				$active_list = '<lh><strong><u>Classmates';
			}
			$active_list .= '</u></strong><img id="image1" src="'.$CFG->wwwroot.'/theme/ECPI/pix/mod/glossary/';
			if (isset($cookie_students) && ($cookie_students == '\'closed\'')){
				$active_list .= 'export';
			}else{
				$active_list .= 'minus';
			}
			$active_list .= '.gif" alt="hide" title="Hide Disabled Students" style="display:block; float:right;" onClick="swapImage(\'students\', \'image1\', \'Students\');"></lh><div id="students" style="display:';
			if (isset($cookie_students) && ($cookie_students == '\'closed\'')){
				$active_list .= 'none';
			}else{
				$active_list .= 'block';
			}
			$active_list .= ';"><li>&nbsp;</li>';
			$teacher_list = '<li>&nbsp;</li><lh><strong><u>Instructor</u></strong><img id="image2" src="'.$CFG->wwwroot.'/theme/ECPI/pix/mod/glossary/';
			if (isset($cookie_teachers) && ($cookie_teachers == '\'closed\'')){
				$teacher_list .= 'export';
			}else{
				$teacher_list .= 'minus';
			}
			$teacher_list .= '.gif" alt="hide" title="Hide Disabled Students" style="display:block; float:right;" onClick="swapImage(\'teachers\', \'image2\', \'Teachers\');"></lh><div id="teachers" style="display:';
			if (isset($cookie_teachers) && ($cookie_teachers == '\'closed\'')){
				$teacher_list .= 'none';
			}else{
				$teacher_list .= 'block';
			}
			$teacher_list .= ';"><li>&nbsp;</li>';

			//initialize user arrays
			$dis_users = array();
			$stu_users = array();
			$teacher_users = array();
			
			//Get the list of disabled students. We won't let students see this.
			$results = get_records_sql($sql.$order);
			foreach($results as $all_enrolled_users){
				
					if(($all_enrolled_users->role == 'disabled') || (in_array($all_enrolled_users->id, $site_dlist))){
						$disabled_list .= '<li class="listentry">';
						if(!in_array($all_enrolled_users->id, $site_dlist)){
							$disabled_list .= '<div class="user"><a href="'.$CFG->wwwroot.'/user/view.php?id='.$all_enrolled_users->id.'">';
						}else{
							$disabled_list .= '<div class="user">';
						}
						if(!in_array($all_enrolled_users->id, $site_dlist)){
							$disabled_list .= '<img src="'.$CFG->wwwroot.'/blocks/classmates/images/disabled_course.png" class="userpicture" title="Disabled at Course Level">';
						}else{
							$disabled_list .= '<img src="'.$CFG->wwwroot.'/blocks/classmates/images/disabled_site.png" class="userpicture" title="Disabled at Site Level">';
						}
						if ((isset($cookie_sort) && ($cookie_sort == '\'firstname\'')) || (!has_capability('moodle/course:viewhiddencourses', $context))){
							$disabled_list .= $all_enrolled_users->firstname." ".$all_enrolled_users->lastname;
						}else{
							$disabled_list .= $all_enrolled_users->lastname.", ".$all_enrolled_users->firstname;
						}
						if(!in_array($all_enrolled_users->id, $site_dlist)){
							$disabled_list .= '</a></div>';
						}else{
							$disabled_list .= '</div>';
						}
						$disabled_list .= "</li>\n";
						$dis_users[] = $all_enrolled_users->id;
					}else if($all_enrolled_users->role == 'student'){
						if(!in_array($all_enrolled_users->id, $dlist) && !in_array($all_enrolled_users->id, $site_dlist)){
							$active_list .= '<li class="listentry">';
							$active_list .= '<div class="user"><a href="'.$CFG->wwwroot.'/user/view.php?id='.$all_enrolled_users->id.'&amp;course='.$COURSE->id.'"';
							if(has_capability('moodle/course:viewhiddencourses', $context)){
								$u_lastaccess = '';
								if ($course->id != SITEID) {
									$u_lastaccess = false;
									if ($lastaccess = get_record('user_lastaccess', 'userid', $all_enrolled_users->id, 'courseid', $COURSE->id)){
										$u_lastaccess = date('F j\, Y', $lastaccess->timeaccess); 
									} 
								}
								if($u_lastaccess == ''){
									$u_lastaccess = 'Never';
								}else if($u_lastaccess == date('F j\, Y')){
									$u_lastaccess = 'Today';
								}
								$active_list .= ' title="Last Access: '.$u_lastaccess.'"';
							}
							if($all_enrolled_users->hidden == 0){
								$active_list .= '>'.print_user_picture($all_enrolled_users->id, $COURSE->id, $all_enrolled_users->picture, 16, true, false, '', false);
							}else{
								$active_list .= '><img src="'.$CFG->wwwroot.'/blocks/classmates/images/'.$hiddenpic.'" class="userpicture" height="16" width="16" title="Hidden Role Assignment">';
							}
							if ((isset($cookie_sort) && ($cookie_sort == '\'firstname\'')) || (!has_capability('moodle/course:viewhiddencourses', $context))){
								$active_list .= $all_enrolled_users->firstname." ".$all_enrolled_users->lastname.'</a></div>';
							}else{
								$active_list .= $all_enrolled_users->lastname.", ".$all_enrolled_users->firstname.'</a></div>';
							}
							$active_list .= "</li>\n";
							$stu_users[] = $all_enrolled_users->id;
						}
					}else if(($all_enrolled_users->role == 'editingteacher') || ($all_enrolled_users->role == 'teacher')){
						if(!in_array($all_enrolled_users->id, $dlist) && !in_array($all_enrolled_users->id, $site_dlist)){
							$teacher_list .= '<li class="listentry">';
							$teacher_list .= '<div class="user"><a href="'.$CFG->wwwroot.'/user/view.php?id='.$all_enrolled_users->id.'&amp;course='.$COURSE->id.'">';
							if($all_enrolled_users->hidden == 0){
								$teacher_list .= print_user_picture($all_enrolled_users->id, $COURSE->id, $all_enrolled_users->picture, 16, true, false, '', false);
							}else{
								$teacher_list .= '<img src="'.$CFG->wwwroot.'/blocks/classmates/images/'.$hiddenpic.'" class="userpicture" height="16" width="16" title="Hidden Role Assignment">';
							}
							if ((isset($cookie_sort) && ($cookie_sort == '\'firstname\'')) || (!has_capability('moodle/course:viewhiddencourses', $context))){
								$teacher_list .= $all_enrolled_users->firstname." ".$all_enrolled_users->lastname.'</a>';
							}else{
								$teacher_list .= $all_enrolled_users->lastname.", ".$all_enrolled_users->firstname.'</a>';
							}
							if (($all_enrolled_users->role == 'teacher')&&(has_capability('moodle/course:viewhiddencourses', $context))){
								 $teacher_list .= '<span title="Non-editing Teacher"><img src="'.$CFG->wwwroot.'/blocks/classmates/images/nonedit.png" class="userpicture"></span>';
							}
							$teacher_list .= '</div>';
							$teacher_list .= "</li>\n";
							$teacher_users[] = $all_enrolled_users->id;
						}
				}

			}
			
			$disabled_list .= '</div>';
			$active_list .= '</div>';
			$teacher_list .= '</div>';
			
			if(count($dis_users) == 0){
			$disabled_list = '<li>&nbsp;</li><lh><strong><u>Disabled Students</u></strong></lh><br /><br />No disabled students.';
			}
			if(count($stu_users) == 0){
				$active_list = 'No students enrolled.';
			}
			if(count($teacher_users) == 0){
				$teacher_list = '';
			}
			
			//Print the information to the block
			$this->content->text .= "<ul class='list'>\n";
			$this->content->text .= $active_list.$teacher_list;
			if(has_capability('moodle/course:viewhiddencourses', $context)){
				$this->content->text .= $disabled_list;
			}
            $this->content->text .= '</ul><div class="clearer"><!-- --></div>';
	
		}
		
		if(has_capability('moodle/course:viewhiddencourses', $context)){
			if (isset($cookie_sort) && ($cookie_sort == '\'firstname\'')){
				$this->content->text .= '<hr><a href="#" onClick="setCookie(\''.$USER->username.'_'.$COURSE->shortname.'_sort\', \'lastname\');">Sort by Last Name</a>';
			}else{
				$this->content->text .= '<hr><a href="#" onClick="setCookie(\''.$USER->username.'_'.$COURSE->shortname.'_sort\', \'firstname\');">Sort by First Name</a>';
			}
			$passingarray = urlencode(serialize($stu_users)); 
			
// The following commented code is incomplete. The goal is to integrate this block into the current Moodle messaging system.
//
//
//			$this->content->text .= '<br><form method="POST" action="'.$CFG->wwwroot.'/blocks/classmates/message.php" name="messages">
//											<input type="hidden" value="'.$passingarray.'" name="users">
//											<input type="hidden" value="'.$COURSE->id.'" name="course">
//											<input type="hidden" name="sesskey" value="'.$USER->sesskey.'" />
//											<input type="hidden" name="context" value="'.$context->id.'" />
//											<a href="#" onClick="submitform(\''.$CFG->wwwroot.'/blocks/classmates/message.php\', \'message\');">Send Message</a></form>';
		}
		
		//check if this is a term shell
		if(substr($COURSE->fullname,0,3) == '(20'){
			$c_name = explode(' ', $COURSE->fullname);
			$course_code = $c_name[1];
			$c_section = $c_name[count($c_name)-1];
			$this->title = $course_code.$c_section.' Users';
		}else{
			$this->content->text = '';
		}	
		
        $this->title = 'Users in this course';
        return $this->content;
    }
}

?>
